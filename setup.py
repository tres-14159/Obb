#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
	name = 'Obb',
	
	version = '2016.12.27.20.37',
	
	description = 'Obb is both a real-time strategy game and a tile placement puzzle game.',
	
	long_description =
		'Obb is both a real-time strategy game and a tile placement puzzle game.' + \
		'Control the growth of a mutant named Obb. ' + \
		'Expand into space and defend Obb from hostile forces. ' + \
		'Collect mutagen to form new types of organs.' + \
		'This game was originally written in one week for PyWeek 13, the python game programming contest.'
)